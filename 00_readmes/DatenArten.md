# Intuition: Datenarten

- metrische Daten
- ordinale Daten
- nominale Daten

## Metrische Daten
- Zahlen
- Abstände haben eine "direkte" Bedeutung
- meist stetig (es gibt Kommawerte dazwischen)
- Beipiel: Distanz
    - 2km + 1km = 3km

### Umgang mit metrischen Datenarten
- direkte Verwendung in linearer Regression

## Ordinale Daten
- es gitb eine Rangordnung / natürliche Ordnung
- die Abstände haben eine unterschiedliche / unbestimmte Größe
- Beispiel: Schulnoten:
    - Note 1: sehr gut
    - Note 2: gut

- Beispiel:
    - Morgens
    - Mittags
    - Abends

### Umgang mit ordinalen Daten
- Option 1: Auffassung es seien metrische Daten
    - Morgens => 1
    - Mittags => 2
    - Abends => 3
    - das Modell lernt aber mit einer falschen Struktur (siehe Schulnoten: 
        Unterschied zwischen 4 und 5 (Bestehen und Nichtbestehen) oder 1 und 2 
        (sehr gut und gut). Somit ist der Unterschied zwischen 4 und 5 ein 
        größerer / bedeutenderer Unterschied)
- Option 2: Behandlung wie nominale Daten
    - Ergebnis: wir haben sehr viel mehr Spalten
        => mehr Dimensionen, mehr Parameter, die trainiert werden müssen


## Nominale Daten
- es gibt keine Rangordnung
- Beispiel:
    - welches OS benutzt du?
    - Geschlecht
    - Studiengang

### Umgang mit nominalen Daten
- Aufbereitung der Daten vor Verwendung in linearer Regression
- Beispiel:
    - Welches Fach studierst du mit entsprechender Repräsentation:
        - Informatik => 1
        - Mathematik => 2
        - Physik => 3
    - benötige hierbei das One-Hot-Encoding