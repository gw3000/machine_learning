# Klassifizierung

Daten sollen in erstellte Kategorien gepackt werden.

## Beispiele für eine (binäre) Klassifizierung
    - Ist ein Pilz essbar oder nicht?
    - Wird ein Bewerber erfolgreich sein?
    - Ist ein Tumor gutartig oder bösartig?
    - Hat eine Person eine Erkrankung oder nicht?

- Was ist auf einem Bild zu sehen:
    - ein Schuh
    - ein T-Shirt
    - eine Jeans

- Was für eine Ziffer wurde geschrieben?
    - 0...9?

## Vorgang
1. Trainieren des Modells mit bekannten Daten
2. Anschließend kann das Modell mit unbekannten Daten Vorhersagen treffen.