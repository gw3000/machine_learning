# statistische Grundlagen

## Mittelwert / Durchschnitt

- Vorgang:
    1. Aufsummieren aller Werte
    2. Summe wird durch die Anzahl der Elemente geteilt

- Beispiel:
    - 13°C, 15°C, 20°C
    - Mittelwert: (13°C+15°C+20°C)/3 = 48°C/3 = 16°C
   
## Median

- Beispiel:
    - 15°C, 20°C, 15°C
    - Sortieren: 13°C, 15°C, 20°C
    - Median: 15°C
    
## Stichprobe

- Beispiel:
    - Durchschnittseinkommen in Deutschland schätzen
    - Anruf bei  10000 zufällig ausgewählten Leuten und frage nach dem Einkommen

- Begriffe:
    - alle Personen in Deutschland = "Grundgesamtheit"
    - Personen, die ich angerufen habe = "Stichprobe"

## Varianz und Standardabweichung

- Abschätzen wie nah oder weit liegen Daten voneinander
- recht schwer zu interpretieren

- die Standardabweichung ist die Wurzel der Varianz
- die durchschnittliche Entfernung zum Mittelwert s (Standardabweichnung)